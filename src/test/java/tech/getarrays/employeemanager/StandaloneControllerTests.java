package tech.getarrays.employeemanager;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import tech.getarrays.employeemanager.model.Employee;
import tech.getarrays.employeemanager.resource.EmployeeResource;
import tech.getarrays.employeemanager.service.EmployeeService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeResource.class)
public class StandaloneControllerTests {

    @MockBean
    EmployeeService employeeService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testfindAll() throws Exception {
        Employee employee = new Employee((long) 1,"bob", "bob@gmail.com", "engineer", "123456789", "pic.jpg", "code");
        List<Employee> employees = Arrays.asList(employee);

        Mockito.when(employeeService.findAllEmployees()).thenReturn(employees);

        mockMvc.perform(get("/API/employee/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("bob")));
    }

    @Test
    public void testgetEmployeeById() throws Exception {
        Employee employee = new Employee((long) 1,"bob", "bob@gmail.com", "engineer", "123456789", "pic.jpg", "code");
//        LiEmployee employees = Arrays.asList(employee);

        Mockito.when(employeeService.findEmployeeById((long) 1)).thenReturn(employee);

        mockMvc.perform(get("/API/employee/find/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is("bob")));
    }

}