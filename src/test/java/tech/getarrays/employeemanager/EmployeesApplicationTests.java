package tech.getarrays.employeemanager;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tech.getarrays.employeemanager.resource.EmployeeResource;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EmployeesApplicationTests {

    @Autowired
    EmployeeResource employeeResource;

    @Test
    public void contextLoads() {
        Assertions.assertThat(employeeResource).isNot(null);
    }
}
